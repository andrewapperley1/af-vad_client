//
//  AFVDFontStatics.h
//  Video-A-Day
//
//  Created by Andrew Apperley on 2/11/2014.
//  Copyright (c) 2014 AFApps. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kAFVDNormalFont;
