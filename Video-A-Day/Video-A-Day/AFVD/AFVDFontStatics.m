//
//  AFVDFontStatics.m
//  Video-A-Day
//
//  Created by Andrew Apperley on 2/11/2014.
//  Copyright (c) 2014 AFApps. All rights reserved.
//

#import "AFVDFontStatics.h"

NSString* const kAFVDNormalFont = @"Helvetica";