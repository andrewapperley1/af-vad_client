//
//  AFVDInitialViewStatics.m
//  Video-A-Day
//
//  Created by Andrew Apperley on 2/11/2014.
//  Copyright (c) 2014 AFApps. All rights reserved.
//

#import "AFVDInitialViewStatics.h"

NSInteger const kAFVDTitleXYBuffer                  = 25;
NSInteger const kAFVDFormItemXBuffer                = 25;
NSInteger const kAFVDFormItemYBuffer                = 35;
NSInteger const kAFVDSigninFormItemYBuffer          = 30;
NSInteger const kAFVDCloseArrowYMargin              = 9;
NSInteger const kAFVDFormItemInputYBufferSize       = 8;
NSInteger const kAFVDFormScrollViewYMargin          = 25;
NSInteger const kAFVDFormTextViewWidth              = 262;
NSInteger const kAFVDFormTextViewHeight             = 27;
NSInteger const kAFVDDraggingViewHeight             = 29;
NSInteger const kAFVDSubmitButtonBottomMargin       = 7;

NSString* const kAFVDLogoImage                      = @"AFVDLogo";
NSString* const kAFVDSigninImage                    = @"AFVDSigninButton";
NSString* const kAFVDSigninSelectedImage            = @"signinButton_selected";
NSString* const kAFVDSignupSelectedImage            = @"signupButton_selected";
NSString* const kAFVDSignupImage                    = @"AFVDSignupButton";
NSInteger const kAFVDCatchPhraseYBuffer             = 20;
NSInteger const kAFVDLogoYBuffer                    = 53;
NSInteger const kAFVDSignupAndSigninXBuffer         = 55;
NSInteger const kAFVDSignupAndSigninYBuffer         = 47;
NSInteger const kAFVDSignupAndSigninLabelYBuffer    = 11;
NSInteger const kAFVDSlidingPanelHeight             = 380;
CGFloat const kPanelViewAnimationDuration           = 0.4f;
NSInteger const kAFVDPanelSignupLabelFontSize       = 20;

CGFloat const kAFVDSnapPanelAnimationSpeed          = 0.4f;
CGFloat const kAFVDSnapPanelAnimationSlowSpeed      = 0.3f;
NSInteger const kAFVDSnapPanelVelocityHighThreshold = 50;
NSInteger const kAFVDSnapPanelVelocityLowThreshold  = 10;
CGFloat const kAFVDSlidingPanelAlpha                = 0.98f;
CGFloat const kAFVDInitialViewTextFieldBorder       = 1.5f;
CGFloat const kAFVDSignupNextPageDuration           = 0.3f;
CGFloat const kAFVDSignupNextPageDelay              = 0.2f;
