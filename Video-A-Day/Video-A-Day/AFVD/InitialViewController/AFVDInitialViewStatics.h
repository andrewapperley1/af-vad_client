//
//  AFVDInitialViewStatics.h
//  Video-A-Day
//
//  Created by Andrew Apperley on 2/11/2014.
//  Copyright (c) 2014 AFApps. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger const kAFVDTitleXYBuffer;
extern NSInteger const kAFVDFormItemXBuffer;
extern NSInteger const kAFVDFormItemYBuffer;
extern NSInteger const kAFVDSigninFormItemYBuffer;
extern NSInteger const kAFVDCloseArrowYMargin;
extern NSInteger const kAFVDFormItemInputYBufferSize;
extern NSInteger const kAFVDFormScrollViewYMargin;
extern NSInteger const kAFVDFormTextViewWidth;
extern NSInteger const kAFVDFormTextViewHeight;
extern NSInteger const kAFVDDraggingViewHeight;
extern NSInteger const kAFVDSubmitButtonBottomMargin;

extern NSString* const kAFVDLogoImage;
extern NSString* const kAFVDSigninImage;
extern NSString* const kAFVDSigninSelectedImage;
extern NSString* const kAFVDSignupSelectedImage;
extern NSString* const kAFVDSignupImage;
extern NSInteger const kAFVDCatchPhraseYBuffer;
extern NSInteger const kAFVDLogoYBuffer;
extern NSInteger const kAFVDSignupAndSigninXBuffer;
extern NSInteger const kAFVDSignupAndSigninYBuffer;
extern NSInteger const kAFVDSignupAndSigninLabelYBuffer;
extern NSInteger const kAFVDSlidingPanelHeight;
extern CGFloat const kPanelViewAnimationDuration;
extern NSInteger const kAFVDPanelSignupLabelFontSize;

extern CGFloat const kAFVDSnapPanelAnimationSpeed;
extern CGFloat const kAFVDSnapPanelAnimationSlowSpeed;
extern NSInteger const kAFVDSnapPanelVelocityHighThreshold;
extern NSInteger const kAFVDSnapPanelVelocityLowThreshold;
extern CGFloat const kAFVDSlidingPanelAlpha;
extern CGFloat const kAFVDInitialViewTextFieldBorder;
extern CGFloat const kAFVDSignupNextPageDuration;
extern CGFloat const kAFVDSignupNextPageDelay;
