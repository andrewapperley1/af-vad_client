//
//  AFVDActivityIndicator.h
//  Video-A-Day
//
//  Created by Andrew Apperley on 1/7/2014.
//  Copyright (c) 2014 AFApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFVDActivityIndicator : UIView

- (void)start;
- (void)stop;

@end