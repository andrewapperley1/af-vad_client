//
//  AFVDVideoRelationshipModel.h
//  Video-A-Day
//
//  Created by Jeremy Fuellert on 11/16/2013.
//  Copyright (c) 2013 AFApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFVDVideoRelationshipModel : NSObject

@property (nonatomic, strong, readonly) NSArray *playlistIds;        //Contains playlist id / username key value pairs.

@end
