//
//  AFVDBaseNetworkStatics.m
//  Video-A-Day
//
//  Created by Jeremy Fuellert on 11/27/2013.
//  Copyright (c) 2013 AFApps. All rights reserved.
//

#import "AFVDBaseNetworkStatics.h"

#pragma mark - Network date formatting
const NSString *kAFVDBaseNetworkDefaultDateFormat                       = @"EEE, dd MMM yyyy hh:mm:ss z";
const NSString *kAFVDBaseNetworkDefaultDateDisplayMonthDayYearFormat    = @"MMM DD yyyy";
const NSString *kAFVDBaseNetworkDefaultDateDisplayHourMinutesFormat     = @"hh:mm:ss z";