//
//  AFVDPopoutMenuStatics.h
//  Video-A-Day
//
//  Created by Andrew Apperley on 2/20/2014.
//  Copyright (c) 2014 AFApps. All rights reserved.
//

extern NSString* const kAFVDPopoutMenuCollectionViewCellId;
extern NSInteger const kAFVDPopoutMenuCollectionViewCellIconSize;
extern NSString* const kAFVDPopoutMenuCloseMenuItemIconImage;
extern NSString* const kAFVDPopoutMenuCloseSelector;
extern CGFloat const kAFVDPopoutMenuCloseDuration;