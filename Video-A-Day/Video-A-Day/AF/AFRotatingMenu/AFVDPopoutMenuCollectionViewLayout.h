//
//  AFRotatingMenuCollectionViewLayout.h
//  Video-A-Day
//
//  Created by Andrew Apperley on 2/20/2014.
//  Copyright (c) 2014 AFApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFVDPopoutMenuCollectionViewLayout : UICollectionViewFlowLayout

@end