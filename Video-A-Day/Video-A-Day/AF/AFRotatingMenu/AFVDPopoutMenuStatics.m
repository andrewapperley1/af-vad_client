//
//  AFVDPopoutMenuStatics.m
//  Video-A-Day
//
//  Created by Andrew Apperley on 2/20/2014.
//  Copyright (c) 2014 AFApps. All rights reserved.
//

#import "AFVDPopoutMenuStatics.h"

NSString* const kAFVDPopoutMenuCollectionViewCellId                 = @"kAFVDPopoutMenuCollectionViewCellId";
NSInteger const kAFVDPopoutMenuCollectionViewCellIconSize           = 50;

NSString* const kAFVDPopoutMenuCloseMenuItemIconImage               = @"AFVDPopoutMenuCloseMenuItemIconImage";
NSString* const kAFVDPopoutMenuCloseSelector                        = @"closeButtonPressed";
CGFloat const kAFVDPopoutMenuCloseDuration                          = 0.6f;